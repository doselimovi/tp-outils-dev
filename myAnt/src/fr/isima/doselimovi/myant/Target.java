package fr.isima.doselimovi.myant;

import fr.isima.doselimovi.myant.tasks.Task;

import java.util.ArrayList;

public class Target {

    private String name;
    private Project project;
    private ArrayList<String> dependencies;
    private ArrayList<Task> tasks;

    public Target(String name) {
        this.name = name;
        this.dependencies = new ArrayList<>();
        this.tasks = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public void addDependency(String targetName) {
        dependencies.add(targetName);
    }

    public ArrayList<String> getDependencies() {
        return dependencies;
    }

    public void addTask(Task task) {
        tasks.add(task);
    }

    public ArrayList<Task> getTasks() {
        return tasks;
    }

    public void execute() {
        for (Task t : tasks) {
            t.execute();
        }
    }
}
