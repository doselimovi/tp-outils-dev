package fr.isima.doselimovi.myant;

import java.io.IOException;
import java.text.ParseException;

public class Main {

    public static void main(String[] args) {
        Project p = new Project("test_build");
        try {
            p.loadFromFile("test_build.txt");
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        p.execute();
    }
}
