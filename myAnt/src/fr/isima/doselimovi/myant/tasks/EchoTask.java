package fr.isima.doselimovi.myant.tasks;

import java.util.Map;

public class EchoTask extends Task {

    public static final String[] ARGUMENTS = {"message"};

    private String message;

    public EchoTask(Map<String, String> args) throws IllegalArgumentException {
        super(ARGUMENTS, args);
        message = args.get("message");
        if (message.equals("")) {
            throw new IllegalArgumentException("Echo task message must not be empty");
        }
    }

    public String getMessage() {
        return message;
    }

    public void execute() {
        System.out.println(message);
    }
}
