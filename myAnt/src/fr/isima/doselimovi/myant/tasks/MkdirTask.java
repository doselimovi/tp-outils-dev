package fr.isima.doselimovi.myant.tasks;

import fr.isima.doselimovi.myant.Target;

import java.io.File;
import java.util.Map;

public class MkdirTask extends Task {

    public static final String[] ARGUMENTS = {"dir"};

    private String dir;

    public MkdirTask(Map<String, String> args) {
        super(ARGUMENTS, args);
        dir = args.get("dir");
    }

    public void execute() {
        System.out.println(dir);
        File f = new File(dir);
        f.mkdir();
    }
}
