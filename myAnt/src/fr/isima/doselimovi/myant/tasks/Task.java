package fr.isima.doselimovi.myant.tasks;

import java.util.Map;

public abstract class Task {

    public Task(String[] requiredArgs, Map<String, String> args) throws IllegalArgumentException {
        for (String arg : requiredArgs) {
            if (!args.containsKey(arg)) {
                throw new IllegalArgumentException("Missing argument");
            }
        }
    }

    public abstract void execute();
}
