package fr.isima.doselimovi.myant.tasks;

import fr.isima.doselimovi.myant.Target;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.Map;

public class CopyTask extends Task {

    public static final String[] ARGUMENTS = {"from", "to"};

    private String from;
    private String to;

    public CopyTask(Map<String, String> args) {
        super(ARGUMENTS, args);
        from = args.get("from");
        to = args.get("to");
    }

    public void execute() {
        try {
            FileUtils.copyFile(new File(from), new File(to));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
