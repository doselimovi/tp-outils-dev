package fr.isima.doselimovi.myant;

import fr.isima.doselimovi.myant.tasks.*;

import java.io.*;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Project {

    private String name;
    private String defaultTarget;
    private HashMap<String, Target> targets;
    private HashMap<String, Class> tasks;

    private static final String COMMENT_REGEX    = "^#.*";
    private static final String TARGET_REGEX     = "(?<name>[A-Za-z]+):(?<deps>[, a-zA-Z]*)";
    private static final String TARGET_DEP_REGEX = "(?<dep>[A-Za-z]+)[ ,]*";
    private static final String TASK_REGEX       = "(?<name>[A-Za-z]+)\\[(?<args>[^]]+)\\]";
    private static final String TASK_ARG_REGEX   = "(?<name>[A-Za-z]+):\"(?<value>[^\"]+)\"";
    private static final String USE_REGEX        = "use (?<class>[.A-Za-z0-9]+) as (?<alias>[, a-zA-Z0-9]*)";

    public Project(String name) {
        this.name = name;
        this.defaultTarget = null;
        this.targets = new HashMap<>();
        this.tasks = new HashMap<>();
    }

    public void loadFromFile(String filename) throws IOException, ParseException {
        loadFromReader(new FileReader(filename));
    }

    public void loadFromReader(Reader in) throws IOException, ParseException {
        BufferedReader br = new BufferedReader(in);

        Target target = null;

        for (String line; (line = br.readLine()) != null; ) {
            if (line.equals("")) {
                continue;

            } else if (line.matches(COMMENT_REGEX)) {
                continue;

            } else if (line.matches(USE_REGEX)) {
                parseUse(line);

            } else if (line.matches(TARGET_REGEX)) {
                target = parseTarget(line);
                targets.put(target.getName(), target);
                if (defaultTarget == null) {
                    defaultTarget = target.getName();
                }

            } else if (line.matches(TASK_REGEX)) {
                if (target == null) {
                    throw new ParseException("Task definition before target definition", 0);
                }
                Task task = parseTask(line);
                target.addTask(task);

            } else {
                throw new ParseException("Line is neither a target nor a task definition", 0);
            }
        }
    }

    public void parseUse(String line) throws ParseException {
        Matcher matcher = Pattern.compile(USE_REGEX).matcher(line);
        if (!matcher.find()) {
            throw new ParseException("Use statement did not match pattern", 0);
        }

        String className = matcher.group("class");
        String alias = matcher.group("alias");

        try {
            Class c = Class.forName(className);
            tasks.put(alias, c);
        } catch (ClassNotFoundException e) {
            throw new ParseException("Class not found", 0);
        }
    }

    public Target parseTarget(String line) throws ParseException {
        Matcher matcher = Pattern.compile(TARGET_REGEX).matcher(line);
        if (!matcher.find()) {
            throw new ParseException("Target did not match pattern", 0);
        }

        String targetName = matcher.group("name");
        String targetDependencies = matcher.group("deps");

        Target target = new Target(targetName);

        Matcher depMatcher = Pattern.compile(TARGET_DEP_REGEX).matcher(targetDependencies);
        while (depMatcher.find()) {
            target.addDependency(depMatcher.group("dep"));
        }

        return target;
    }

    public Task parseTask(String line) throws ParseException {
        Matcher matcher = Pattern.compile(TASK_REGEX).matcher(line);
        if (!matcher.find()) {
            throw new ParseException("Task did not match pattern", 0);
        }

        String taskName = matcher.group("name");
        if (!tasks.containsKey(taskName)) {
            throw new ParseException("Unknown task", 0);
        }
        Class<Task> taskClass = tasks.get(taskName);

        String taskArgs = matcher.group("args");
        HashMap<String, String> args = new HashMap<>();
        Matcher argsMatcher = Pattern.compile(TASK_ARG_REGEX).matcher(taskArgs);
        while (argsMatcher.find()) {
            if (args.containsKey(argsMatcher.group("name"))) {
                throw new ParseException("Task argument already exists", 0);
            }
            args.put(argsMatcher.group("name"), argsMatcher.group("value"));
        }

        Task task;
        try {
            task = taskClass.getConstructor(Map.class).newInstance(args);
        } catch (Exception e) {
            throw new ParseException("Internal error", 0);
        }

        return task;
    }

    public void execute() {
        execute(defaultTarget);
    }

    public void execute(String targetName) throws IllegalArgumentException {
        if (!targets.containsKey(targetName)) {
            throw new IllegalArgumentException("Invalid target name");
        }
        Target t = targets.get(targetName);
        for (String dep : t.getDependencies()) {
            execute(dep);
        }
        t.execute();
    }

    public String getDefaultTarget() {
        return defaultTarget;
    }

    public HashMap<String, Target> getTargets() {
        return targets;
    }
}
