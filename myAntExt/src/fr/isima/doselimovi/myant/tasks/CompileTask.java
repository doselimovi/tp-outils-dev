package fr.isima.doselimovi.myant.tasks;

import java.io.IOException;
import java.util.Map;

public class CompileTask extends Task {

    public static final String[] ARGUMENTS = {"target"};

    private String target;

    public CompileTask(Map<String, String> args) throws IllegalArgumentException {
        super(ARGUMENTS, args);
        target = args.get("target");
    }

    public void execute() {
        Runtime runtime = Runtime.getRuntime();
        try {
            runtime.exec("javac " + target);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
