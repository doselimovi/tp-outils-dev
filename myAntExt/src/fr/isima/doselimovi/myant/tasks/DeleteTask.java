package fr.isima.doselimovi.myant.tasks;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.util.Map;

public class DeleteTask extends Task {

    public static final String[] ARGUMENTS = {"target"};

    private String target;

    public DeleteTask(Map<String, String> args) throws IllegalArgumentException {
        super(ARGUMENTS, args);
        target = args.get("target");
    }

    public void execute() {
        FileUtils.deleteQuietly(new File(target));
    }
}
