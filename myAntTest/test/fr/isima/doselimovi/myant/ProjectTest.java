package fr.isima.doselimovi.myant;

import com.intellij.util.text.CharSequenceReader;
import fr.isima.doselimovi.myant.tasks.EchoTask;
import fr.isima.doselimovi.myant.tasks.Task;
import org.junit.Test;
import static org.junit.Assert.*;

import java.io.IOException;
import java.text.ParseException;
import java.util.HashMap;

public class ProjectTest {

    private final String STUB_PROJECT_NAME = "TestProject";

    // Use statements parsing tests

    @Test(expected = ParseException.class)
    public void parseUse_InvalidUseStatement() throws ParseException {
        Project p = new Project(STUB_PROJECT_NAME);
        p.parseUse("\n");
    }

    @Test
    public void parseUse_ValidUseStatement() throws ParseException {
        Project p = new Project(STUB_PROJECT_NAME);
        p.parseUse("use fr.isima.doselimovi.myant.tasks.EchoTask as echo");
        p.parseUse("use fr.isima.doselimovi.myant.tasks.CopyTask as cp");
        p.parseUse("use fr.isima.doselimovi.myant.tasks.MkdirTask as mkdir");
    }

    // Target parsing tests

    @Test(expected = ParseException.class)
    public void parseTarget_InvalidTarget() throws ParseException {
        Project p = new Project(STUB_PROJECT_NAME);
        p.parseTarget("\n");
    }

    @Test
    public void parseTarget_ValidTarget() {
        Project p = new Project(STUB_PROJECT_NAME);

        try {
            Target target = p.parseTarget("target:\n");

            assertEquals("target", target.getName());
            assertEquals(0, target.getDependencies().size());
        } catch (ParseException e) {
            e.printStackTrace();
            fail();
        }
    }

    @Test
    public void parseTarget_ValidTargetWithDependencies() {
        Project p = new Project(STUB_PROJECT_NAME);

        try {
            Target target = p.parseTarget("target: firstDep, secondDep\n");

            assertEquals("target", target.getName());
            assertEquals(2, target.getDependencies().size());
            assertTrue(target.getDependencies().contains("firstDep"));
            assertTrue(target.getDependencies().contains("secondDep"));
        } catch (ParseException e) {
            e.printStackTrace();
            fail();
        }
    }

    // Task parsing tests

    @Test(expected = ParseException.class)
    public void parseTask_InvalidTask() throws ParseException {
        Project p = new Project(STUB_PROJECT_NAME);
        p.parseTask("\n");
    }

    @Test(expected = ParseException.class)
    public void parseTask_UnknownTask() throws ParseException {
        Project p = new Project(STUB_PROJECT_NAME);
        p.parseTask("fakeTask[]\n");
    }

    @Test(expected = ParseException.class)
    public void parseTask_MissingArgument() throws ParseException {
        Project p = new Project(STUB_PROJECT_NAME);
        p.parseTask("echo[]\n");
    }

    @Test(expected = ParseException.class)
    public void parseTask_InvalidArgument() throws ParseException {
        Project p = new Project(STUB_PROJECT_NAME);
        p.parseTask("echo[message:\"\"]\n");
    }

    @Test
    public void parseTask_ValidEchoTask() {
        Project p = new Project(STUB_PROJECT_NAME);

        try {
            p.parseUse("use fr.isima.doselimovi.myant.tasks.EchoTask as echo");
            Task task = p.parseTask("echo[message:\"Text for echo !\"]\n");

            assertEquals(EchoTask.class, task.getClass());
            assertEquals("Text for echo !", ((EchoTask) task).getMessage());
        } catch (ParseException e) {
            e.printStackTrace();
            fail();
        }
    }

    // Project loading tests

    @Test(expected = ParseException.class)
    public void loadFromReader_InvalidInput() throws IOException, ParseException {
        Project p = new Project(STUB_PROJECT_NAME);

        StringBuilder input = new StringBuilder();
        input.append("neitherTargetNorTask\n");

        p.loadFromReader(new CharSequenceReader(input));
    }

    @Test(expected = ParseException.class)
    public void loadFromReader_TaskBeforeTarget() throws IOException, ParseException {
        Project p = new Project(STUB_PROJECT_NAME);

        StringBuilder input = new StringBuilder();
        input.append("echo[message:\"hello\"]\n");

        p.loadFromReader(new CharSequenceReader(input));
    }

    @Test
    public void loadFromReader_ValidTarget() {
        Project p = new Project(STUB_PROJECT_NAME);

        StringBuilder input = new StringBuilder();
        input.append("# this is a comment\n");
        input.append("use fr.isima.doselimovi.myant.tasks.EchoTask as echo\n");
        input.append("\n");
        input.append("firstTarget: secondTarget, thirdTarget\n");
        input.append("echo[message:\"hello firstTarget !\"]\n");
        input.append("\n");

        input.append("# this is a comment\n");
        input.append("\n");

        input.append("fourthTarget:\n");
        input.append("echo[message:\"hello fourthTarget !\"]\n");
        input.append("\n");

        input.append("secondTarget: fourthTarget\n");
        input.append("echo[message:\"hello secondTarget !\"]\n");
        input.append("\n");

        input.append("thirdTarget:\n");
        input.append("echo[message:\"hello thirdTarget !\"]\n");
        input.append("\n");

        try {
            p.loadFromReader(new CharSequenceReader(input));
        } catch (IOException e) {
            e.printStackTrace();
            fail();
        } catch (ParseException e) {
            e.printStackTrace();
            fail();
        }

        assertEquals("firstTarget", p.getDefaultTarget());

        HashMap<String, Target> targets = p.getTargets();

        assertTrue(targets.containsKey("firstTarget"));
        assertTrue(targets.containsKey("secondTarget"));
        assertTrue(targets.containsKey("thirdTarget"));
        assertTrue(targets.containsKey("fourthTarget"));

        Target firstTarget = targets.get("firstTarget");
        assertEquals(2, firstTarget.getDependencies().size());

        Target secondTarget = targets.get("secondTarget");
        assertEquals(1, secondTarget.getDependencies().size());

        Target thirdTarget = targets.get("thirdTarget");
        assertEquals(0, thirdTarget.getDependencies().size());

        Target fourthTarget = targets.get("fourthTarget");
        assertEquals(0, fourthTarget.getDependencies().size());

        assertTrue(firstTarget.getDependencies().contains("secondTarget"));
        assertTrue(firstTarget.getDependencies().contains("thirdTarget"));
        assertTrue(secondTarget.getDependencies().contains("fourthTarget"));

        Task task;

        assertEquals(1, firstTarget.getTasks().size());
        task = firstTarget.getTasks().get(0);
        assertEquals(EchoTask.class, task.getClass());
        assertEquals("hello firstTarget !", ((EchoTask) task).getMessage());

        assertEquals(1, secondTarget.getTasks().size());
        task = secondTarget.getTasks().get(0);
        assertEquals(EchoTask.class, task.getClass());
        assertEquals("hello secondTarget !", ((EchoTask) task).getMessage());

        assertEquals(1, thirdTarget.getTasks().size());
        task = thirdTarget.getTasks().get(0);
        assertEquals(EchoTask.class, task.getClass());
        assertEquals("hello thirdTarget !", ((EchoTask) task).getMessage());

        assertEquals(1, fourthTarget.getTasks().size());
        task = fourthTarget.getTasks().get(0);
        assertEquals(EchoTask.class, task.getClass());
        assertEquals("hello fourthTarget !", ((EchoTask) task).getMessage());
    }
}